#include <vector>
#include <string>
#include <map>
#include <sstream>
#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::string;
using std::map;
using std::vector;

using std::ifstream;
using std::stringstream;
#include "BleakVirtualMachine.h"

const int BleakVirtualMachine::INSTRUCTION_OUT_OF_BOUNDS = 1;
const int BleakVirtualMachine::INPUT_OUT_OF_BOUNDS = 2;
const int BleakVirtualMachine::UNKNOWN_INSTRUCTION = 3;


//constructor
BleakVirtualMachine::BleakVirtualMachine(const string &srcFilePath, const string &inputFilePath) {
	BleakVirtualMachine::Reset();

	//src file is the instructions (ex: add r0 r1)
	//input file are the values to setup the registers
	
	//parse the src file
	ifstream fin;
	fin.open(srcFilePath);
	if(!fin.good()) {
		cout << "ERROR: Source file not found." << endl;
	} else {
		
		string line;
		int lineIndex = 0;
		while(!fin.eof()) {
			getline(fin, line);
			if(line.empty()) { cout << "Empty input line." << endl; }
			else {
				vector<string> tokens = BleakVirtualMachine::Tokenize(line);
							
				//the line has a <- label
				if(tokens.at(tokens.size() - 2).compare("<-") == 0) {
					BleakVirtualMachine::labels[tokens.at(tokens.size() - 1)] = lineIndex;
					stringstream ss;
					for(unsigned int i = 0; i < tokens.size() - 3; i++) {
						ss << tokens.at(i) << " ";
					}
					BleakVirtualMachine::instructions.push_back(ss.str());
				//the line doesn't have a <- label
				} else {
					BleakVirtualMachine::instructions.push_back(line);
				}
			}
			lineIndex++;
		}
		fin.close();
	}

	//parse the input file

	fin.open(inputFilePath);
	if(!fin.good()) {
		cout << "ERROR: Input file not found." << endl;
	} else {

		while(!fin.eof()) {
			//read line
			string line;
			getline(fin, line);
			if(line.empty()) { cout << "empty input line." << endl; }
			else {
				//break up into tokens
				BleakVirtualMachine::input.push_back(std::stoi(line, NULL, 10));
			}
		}
		fin.close();
	}
}

void BleakVirtualMachine::Reset() {
	output.clear();
	BleakVirtualMachine::registers["nc"] = 0;
	BleakVirtualMachine::registers["pc"] = 0;
	BleakVirtualMachine::registers["r0"] = 0;
	BleakVirtualMachine::registers["r1"] = 0;
	BleakVirtualMachine::registers["r2"] = 0;
	BleakVirtualMachine::registers["r3"] = 0;
	BleakVirtualMachine::registers["r4"] = 0;
	BleakVirtualMachine::registers["r5"] = 0;
	BleakVirtualMachine::registers["r6"] = 0;
	BleakVirtualMachine::registers["r7"] = 0;
	BleakVirtualMachine::registers["r8"] = 0;
	BleakVirtualMachine::registers["r9"] = 0;
	BleakVirtualMachine::registers["ra"] = 0;
}

//getters and setters
const vector<string> BleakVirtualMachine::GetInstructions() const {
	return BleakVirtualMachine::instructions;
}

const vector<int> BleakVirtualMachine::GetInput() const {
	return BleakVirtualMachine::input;
}

const vector<int> BleakVirtualMachine::GetOutput() const {
	return BleakVirtualMachine::output;
}

map<string, int>& BleakVirtualMachine::GetRegisters() {
	return BleakVirtualMachine::registers;
}

const map<string, int>& BleakVirtualMachine::GetLabels() const {
	return BleakVirtualMachine::labels;
}

//implementations
string BleakVirtualMachine::ResolveLValue(const string &expr) {
	if(expr.compare(0,1,"r") == 0 || expr.compare("pc") == 0 || expr.compare("ra") == 0 || expr.compare("nc") == 0) {
		return expr;
	} else if(expr.compare(0,1,"[") == 0) {
		stringstream ss;
		ss << "r" << expr.substr(2,1);
		int regNum = BleakVirtualMachine::registers[ss.str()];
		ss.str("");
		ss << "r" << regNum;
		return ss.str();
	} else {
		cout << "Not a valid lvalue." << endl;
		return "";
	}
}

int BleakVirtualMachine::ResolveRValue(const string &expr) {
	cout << "rVal: " << expr << endl;
	if(expr.compare(0,1,"r") == 0 || expr.compare("pc") == 0 || expr.compare("ra") == 0 || expr.compare("nc") == 0) {

		cout << "return: " << BleakVirtualMachine::registers[expr] << endl;
		return BleakVirtualMachine::registers[expr];
	} else if(expr.compare(0,1,"[") == 0) {
		cout << "return: " << BleakVirtualMachine::registers[BleakVirtualMachine::ResolveLValue(expr)] << endl;
		
		return BleakVirtualMachine::registers[BleakVirtualMachine::ResolveLValue(expr)];
	} else {
		try {
			cout << "return: " << stoi(expr, NULL, 10) << endl;
			return stoi(expr, NULL, 10);
		} catch (const std::invalid_argument& ia) {
			std::cerr << "Invalid argument: " << ia.what() << endl;
		}

		return 0;
	}
}

vector<string> BleakVirtualMachine::Tokenize(const string &text) {
	vector<string> vect;
	std::stringstream in(text);
	string token;
	while(in >> token) {
		vect.push_back(token);
	}
	return vect;
}

void BleakVirtualMachine::Step() {

}
