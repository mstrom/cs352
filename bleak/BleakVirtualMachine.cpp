#include "BleakVirtualMachine.h"

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::map;
using std::vector;

using std::ifstream;
using std::stringstream;

const int BleakVirtualMachine::INSTRUCTION_OUT_OF_BOUNDS = 1;
const int BleakVirtualMachine::INPUT_OUT_OF_BOUNDS = 2;
const int BleakVirtualMachine::UNKNOWN_INSTRUCTION = 3;


//constructor
BleakVirtualMachine::BleakVirtualMachine(const string &srcFilePath, const string &inputFilePath) {
	BleakVirtualMachine::Reset();

	//src file is the instructions (ex: add r0 r1)
	//input file are the values to setup the registers
	
	//parse the src file
	ifstream fin;
	fin.open(srcFilePath);
	if(!fin.good()) {
		cout << "ERROR: Source file not found." << endl;
	} else {		
		string line;
		int lineIndex = 0;
		while(!fin.eof()) {
			getline(fin, line);
			if(line.empty()) {/* cout << "Empty input line." << endl;*/ }
			else {
				vector<string> tokens = BleakVirtualMachine::Tokenize(line);
							
				//the line has a <- label
				if(tokens.size() > 1 && tokens.at(tokens.size() - 2).compare("<-") == 0) {
					BleakVirtualMachine::labels[tokens.at(tokens.size() - 1)] = lineIndex;
					BleakVirtualMachine::instructions.push_back(line);
				//the line doesn't have a <- label
				} else {
					BleakVirtualMachine::instructions.push_back(line);
				}
			}
			lineIndex++;

		}
		fin.close();
	}

	//parse the input file

	fin.open(inputFilePath);
	if(!fin.good()) {
		cerr << "ERROR: Input file not found." << endl;
	} else {

		while(!fin.eof()) {
			//read line
			string line;
			getline(fin, line);
			if(line.empty()) {/* cout << "empty input line." << endl; */}
			else {
				//break up into tokens
				BleakVirtualMachine::input.push_back(std::stoi(line, NULL, 10));
			}
		}
		fin.close();
	}
}

void BleakVirtualMachine::Reset() {
	output.clear();
	BleakVirtualMachine::registers["nc"] = 0;
	BleakVirtualMachine::registers["pc"] = 0;
	BleakVirtualMachine::registers["r0"] = 0;
	BleakVirtualMachine::registers["r1"] = 0;
	BleakVirtualMachine::registers["r2"] = 0;
	BleakVirtualMachine::registers["r3"] = 0;
	BleakVirtualMachine::registers["r4"] = 0;
	BleakVirtualMachine::registers["r5"] = 0;
	BleakVirtualMachine::registers["r6"] = 0;
	BleakVirtualMachine::registers["r7"] = 0;
	BleakVirtualMachine::registers["r8"] = 0;
	BleakVirtualMachine::registers["r9"] = 0;
	BleakVirtualMachine::registers["ra"] = 0;
}

//getters and setters
const vector<string> BleakVirtualMachine::GetInstructions() const {
	return BleakVirtualMachine::instructions;
}

const vector<int> BleakVirtualMachine::GetInput() const {
	return BleakVirtualMachine::input;
}

const vector<int> BleakVirtualMachine::GetOutput() const {
	return BleakVirtualMachine::output;
}

map<string, int>& BleakVirtualMachine::GetRegisters() {
	return BleakVirtualMachine::registers;
}

const map<string, int>& BleakVirtualMachine::GetLabels() const {
	return BleakVirtualMachine::labels;
}

//implementations
string BleakVirtualMachine::ResolveLValue(const string &expr) {
	if(expr.compare(0,1,"r") == 0 || expr.compare("pc") == 0 || expr.compare("ra") == 0 || expr.compare("nc") == 0) {
		return expr;
	} else if(expr.compare(0,1,"[") == 0 && expr.compare(1,1,"r") == 0) {
		stringstream ss;
		ss << "r" << expr.substr(2,1);
		int regNum = BleakVirtualMachine::registers[ss.str()];
		ss.str("");
		ss << "r" << regNum;
		return ss.str();
	} else {
		cout << "Not a valid lvalue." << endl;
		return "";
	}
}

int BleakVirtualMachine::ResolveRValue(const string &expr) {
	if(expr.compare(0,1,"r") == 0 || expr.compare("pc") == 0 || expr.compare("ra") == 0 || expr.compare("nc") == 0) {

		return BleakVirtualMachine::registers[expr];
	} else if(expr.compare(0,1,"[") == 0) {
		
		return BleakVirtualMachine::registers[BleakVirtualMachine::ResolveLValue(expr)];
	} else {
		return stoi(expr, NULL, 10);
	}
}

vector<string> BleakVirtualMachine::Tokenize(const string &text) {
	vector<string> vect;
	std::stringstream in(text);
	string token;
	while(in >> token) {
		vect.push_back(token);
	}
	return vect;
}

void BleakVirtualMachine::Step() {
	if(registers["pc"] < (int) instructions.size() && registers["pc"] >= 0) { 
		vector<string> tokens = Tokenize(instructions.at(registers["pc"]));

		if(tokens.at(0).compare("add") == 0) {
			string lValue = ResolveLValue(tokens.at(1));
			registers[lValue] += ResolveRValue(tokens.at(2));
			registers["pc"]++;
		} else if (tokens.at(0).compare("sub") == 0) {
			string lValue = ResolveLValue(tokens.at(1));
			registers[lValue] -= ResolveRValue(tokens.at(2));
			registers["pc"]++;
		} else if (tokens.at(0).compare("inc") == 0) {
			registers[ResolveLValue(tokens.at(1))]++;
			registers["pc"]++;
		} else if (tokens.at(0).compare("dec") == 0) {
			registers[ResolveLValue(tokens.at(1))]--;
			registers["pc"]++;
		} else if (tokens.at(0).compare("input") == 0) {
			if(registers["nc"] > (int) input.size() - 1 || registers["nc"] < 0) {
				throw INPUT_OUT_OF_BOUNDS;
			} else {
				registers[ResolveLValue(tokens.at(1))] = input.at(registers["nc"]);
				registers["nc"]++;
			}
			registers["pc"]++;
		} else if (tokens.at(0).compare("output") == 0) {
			output.push_back(ResolveRValue(tokens.at(1)));
			registers["pc"]++;
		} else if (tokens.at(0).compare("store") == 0) {
			registers[ResolveLValue(tokens.at(1))] = ResolveRValue(tokens.at(2));
			registers["pc"]++;
		} else if (tokens.at(0).compare("jmp") == 0) {
			registers["pc"] = labels[tokens.at(1)];
		} else if (tokens.at(0).compare("jpos") == 0) {
			if(ResolveRValue(tokens.at(1)) > 0) {
				registers["pc"] = labels[tokens.at(2)];
			} else {
				registers["pc"]++;
			}
		} else if (tokens.at(0).compare("jneg") == 0) {
			if(ResolveRValue(tokens.at(1)) < 0) {
				registers["pc"] = labels[tokens.at(2)];
			} else {
				registers["pc"]++;
			}
		} else if (tokens.at(0).compare("jzilch") == 0) {
			if(ResolveRValue(tokens.at(1)) == 0) {
				registers["pc"] = labels[tokens.at(2)];
			} else {
				registers["pc"]++;			
			}
		} else if (tokens.at(0).compare("call") == 0) {
			registers["ra"] = (registers["pc"] + 1);
			registers["pc"] = labels[tokens.at(1)];
		} else if (tokens.at(0).compare("return") == 0) {
			registers["pc"] = registers["ra"];
		} else {
			throw UNKNOWN_INSTRUCTION;
		}
		
	} else { throw INSTRUCTION_OUT_OF_BOUNDS; }
}


void BleakVirtualMachine::vectorPrint(vector<string> vect) const {
	cout << endl;
	for(unsigned int i = 0; i < vect.size(); i++) {
		cout << vect.at(i) << " ";
	}
	cout << endl;
}
