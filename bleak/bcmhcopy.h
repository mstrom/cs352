class BleakVirtualMachine {

	private:
	std::vector<std::string> instructions;
	std::vector<int> input;
	std::vector<int> output;
	std::map<std::string, int> registers;
	std::map<std::string, int> labels;

	public:
	static const int INSTRUCTION_OUT_OF_BOUNDS;
	static const int INPUT_OUT_OF_BOUNDS;
	static const int UNKNOWN_INSTRUCTION;

	//constructor
	BleakVirtualMachine(const std::string &srcFilePath, const std::string &inputFilePath);
	
	//methods
	void Reset();
	const std::vector<std::string> GetInstructions() const;
	const std::vector<int> GetInput() const;
	const std::vector<int> GetOutput() const;
	std::map<std::string, int>& GetRegisters();
	const std::map<std::string, int>& GetLabels() const;
	std::string ResolveLValue(const std::string &expr);
	int ResolveRValue(const std::string &expr);
	static std::vector<std::string> Tokenize(const std::string &text);
	void Step();
};
