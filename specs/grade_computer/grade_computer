#!/usr/bin/env zsh

# ---------------------------------------------------------------------------- 

iskey=0
isgrader=0
onlylater=0
while getopts kgl opt; do
  case $opt in
    (k)
      iskey=1
      ;;
    (g)
      isgrader=1
      ;;
    (l)
      onlylater=1
      ;;
  esac
done

graderdir=${0:A:h}
srcdir=$(pwd)
basedir=${srcdir:t}
libdir=../.tmp
name="computer"

mkdir -p $libdir

# Assert that we're in the right directory.
if [[ $basedir != $name ]]; then
  echo "Oh no! The grader must be run from the $name directory." >&2
  exit 1
fi

# ---------------------------------------------------------------------------- 

# Assert that all required files exist.
# for f in $files; do
  # if [[ ! -f $f.hdl ]]; then
    # echo "Whoops! Expected file $f.hdl not found." >&2
    # exit 1
  # fi
# done

# ---------------------------------------------------------------------------- 

nand2tetrisdir=$libdir/nand2tetris
if [[ ! -d $nand2tetrisdir ]]; then
  echo "Downloading Nand2Tetris software suite..."
  if [[ $isgrader -eq 0 ]]; then
    out=/dev/stdout
  else
    out=/dev/null
  fi
  (cd $libdir &&
   wget http://www.nand2tetris.org/software/nand2tetris.zip &&
   unzip nand2tetris.zip) >& $out
  echo
fi

\cp -f $graderdir/MemoryNoKB.cmp $libdir/nand2tetris/projects/05
\cp -f $graderdir/MemoryNoKB.tst $libdir/nand2tetris/projects/05
\cp -f $graderdir/Keyboard.class $libdir/nand2tetris/tools/builtInChips

# echo "Copying over Or3 and Majority tests..."
# \cp -f $graderdir/Or3.{tst,cmp} $graderdir/Majority.{tst,cmp} $nand2tetrisdir/projects/01
# echo

# ---------------------------------------------------------------------------- 

simulator=${libdir:A}/nand2tetris/tools/HardwareSimulator.sh

errors=(0 0)
level=1
files=(05/Memory 05/CPU 05/Computer)
for ff in $files; do
  f=${ff:t}
  dir=${ff:h}
  ndir=$libdir/nand2tetris/projects/$dir

  echo "Testing $f..."
  if [[ ! -f $f.hdl ]]; then
    echo "File $f.hdl could not be found."
    errors[$level]=1
  else
    echo "Copying $f.hdl to $libdir/nand2tetris/projects/$dir for testing."
    \cp -f $f.hdl $ndir

    if [[ $f == 'Memory' ]]; then
      tests=(MemoryNoKB)
    elif [[ $f == 'CPU' ]]; then
      tests=(CPU CPU-external)
    else
      tests=(ComputerAdd ComputerAdd-external ComputerMax ComputerMax-external ComputerRect ComputerRect-external)
    fi

    for test in $tests; do
      echo
      echo "Executing \"source $libdir/nand2tetris/tools/HardwareSimulator.sh $test.tst\""
      (cd $ndir && source $simulator $test.tst > /dev/null)
      okstatus=$?
      if [[ $okstatus -eq 0 ]]; then
        echo "RESULT: OK!"
      else
        echo "RESULT: FAILED!"
        
        if [[ $isgrader -eq 0 ]]; then
          echo -n "Run vimdiff? (Hit :qa to quit.) [y] or n: "
          read answer
          if [[ "$answer" != "n" ]]; then
            (cd $ndir && vimdiff -b -R $f.cmp $f.out)
          fi
        fi

        errors[$level]=1
      fi
    done
  fi
  echo
done

# ---------------------------------------------------------------------------- 

if [[ $errors[1] -gt 0 ]]; then
  exit 1
fi

if [[ $errors[2] -gt 0 ]]; then
  exit 2
fi

# Assert that a commit and push has happened or is about to happen.
if [[ $isgrader -eq 0 ]]; then
  echo "Have you added any unadded files, committed, and pushed to Bitbucket?"
  echo -n "Run \"git status\" if you don't know. y or [n]? "
  read answer
  if [[ "$answer" != "y" ]]; then
    exit 2
  fi
  echo
fi

exit 0
