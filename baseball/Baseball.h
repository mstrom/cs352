#include <string>
#include <vector>
#include <map>

class Baseball {

public:
	static unsigned int EvaluateDigit(char c);
	static char ValueToDigit(unsigned char c);
	static unsigned int FromBase(const std::string &token, 
					unsigned int base);
	static std::string ToBase(unsigned int value,
				unsigned int base);
	static unsigned int EvaluateLiteral(const std::string &token);
	const unsigned int EvaluateVariable(const std::string &varID);
	const unsigned int EvaluateTerm(const std::string &token);
	const unsigned int EvaluateOperation(const std::string &left,
					const std::string &operation,
					const std::string &right);
	static std::vector<std::string> Tokenize(const std::string &exp);
	const unsigned int ExtractTargetBase(std::vector<std::string> &expTokens);
	static std::string ExtractTargetID(std::vector<std::string> &expTokens);
	unsigned int Evaluate(const std::vector<std::string> &expTokens);
	std::string Evaluate(const std::string &exp);

private:
	std::map<std::string, unsigned int> varmap;
};		

