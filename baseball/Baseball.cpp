#include <iostream>
#include <sstream>
#include <math.h>
#include <map>
#include "Baseball.h"

unsigned int Baseball::EvaluateDigit(char c) {
	if ((c < 48) || ((c > 57) && (c < 65)) || ((c > 90) && (c < 97)) || (c > 122)) {
		std::stringstream out;
		out << "Illegal digit: " << c << ". Please send a valid value";
		std::cout << out.str();
		throw out.str();
	} else if ((c >= 48) && (c <= 57)) {
		return c - 48;
	} else if ((c >= 65) && (c <= 90)) {
		return c - 55;
	} else if ((c >= 97) && (c <= 122)) {
		return c - 87;
	}

	return 0;
}

char Baseball::ValueToDigit(unsigned char c) {
	if ((c < 0) || (c > 35)) {
		std::stringstream out;
		out << "Error: value: " << c << " is out of bounds [0,35].";
		std::cout << out.str();
		throw out.str();
	} else if ((c >= 0) && (c <= 9)) {
		return c + 48;
	} else if ((c >= 10) && (c <= 35)) {
		return c + 87;
	}

	return ' ';
}

unsigned int Baseball::FromBase(const std::string &token, unsigned int base) {
	unsigned int i;
	int power = token.length() - 1;
	int value = 0;

	for (i = 0; i < token.length(); i++) {
		value = value + ((unsigned long int) pow(base, power) * EvaluateDigit(token.at(i)));
		power = power - 1;
	}
	return value;
}

std::string Baseball::ToBase(unsigned int value, unsigned int base) {
	//find length
	unsigned int power = 0;
	while(value >= (unsigned long int) pow(base, power + 1)) {
		power++;
	}
	
	//make number
	std::stringstream out;
	unsigned int temp = value;
	unsigned int number = 0;
	int i = power;
	while(i >= 0) {
		number = temp / (unsigned long int) pow(base, power);
		temp = temp - (number * (unsigned long int) pow(base, power));
		out << ValueToDigit(number);

		power--;
		i--;
	}
	return out.str();
}

unsigned int Baseball::EvaluateLiteral(const std::string &token) {
	unsigned int index;
	unsigned int i = 0;
	bool isDec = true;
	unsigned int value;
	std::string tempvalue;
	std::string tempbase;
	unsigned int base;

	for(i = 0; i < token.length(); i++) {
		if(token.at(i) == '_') {
			isDec = false;
			index = i;
		}
	}
	
	if(isDec) {
		value = FromBase(token, 10);
	} else {
		tempvalue = token.substr(0, index);

		tempbase = token.substr(index + 1, token.length());
		base = FromBase(tempbase, 10);
		
		value = FromBase(tempvalue, base);
	}
	return value;
}

const unsigned int Baseball::EvaluateVariable(const std::string &varID) {
	return varmap.at(varID);

}

const unsigned int Baseball::EvaluateTerm(const std::string &token) {
	unsigned int value;
	std::string temp;

	if(token.at(0) == '~') {
		temp = token.substr(1);
		if(temp.at(0) == ':') {
			value = EvaluateVariable(temp);
			return ~value;
		} else{
			value = EvaluateLiteral(temp);
			return ~value;
		}
	} else {
		if(token.at(0) == ':') {
			value = EvaluateVariable(token);
			return value;
		} else {
			value = EvaluateLiteral(token);
			return value;
		}
	}
}

const unsigned int Baseball::EvaluateOperation(const std::string &left,
					const std::string &operation,
					const std::string &right) {
	if(operation.compare("+") == 0) {
		return EvaluateTerm(left) + EvaluateTerm(right);
	} else if(operation.compare("-") == 0) {
		return EvaluateTerm(left) - EvaluateTerm(right);
	} else if(operation.compare("&") == 0) {
		return EvaluateTerm(left) & EvaluateTerm(right);
	} else if(operation.compare("|") == 0) {
		return EvaluateTerm(left) | EvaluateTerm(right);
	} else if(operation.compare("^") == 0) {
		return EvaluateTerm(left) ^ EvaluateTerm(right);
	} else if(operation.compare("<<") == 0) {
		return EvaluateTerm(left) << EvaluateTerm(right);
	} else if(operation.compare(">>") == 0) {
		return EvaluateTerm(left) >> EvaluateTerm(right);
	} else if(operation.compare("*") == 0) {
		return EvaluateTerm(left) * EvaluateTerm(right);
	} else if(operation.compare("/") == 0) {
		return EvaluateTerm(left) / EvaluateTerm(right);
	} else if(operation.compare("%") == 0) {
		return EvaluateTerm(left) % EvaluateTerm(right);
	} else {
		return 0;
	}	
}

std::vector<std::string> Baseball::Tokenize(const std::string &exp) {
	std::vector<std::string> vect;
	std::stringstream in(exp);
	std::string token;
	while(in >> token) {
		vect.push_back(token);
	}
	return vect;
}

const unsigned int Baseball::ExtractTargetBase(std::vector<std::string> &expTokens) {
	unsigned int base = 10;
	unsigned int i = 0;
	bool is10 = true;
	while(i < expTokens.size()) {
		if(expTokens.at(i).compare("->") == 0) {
			base = EvaluateTerm(expTokens[i + 1]);
			expTokens.pop_back();
			expTokens.pop_back();
			is10 = false;
		}
		i++;
	}
	
	if(is10) { return 10; }
	else { return base; }
}

std::string Baseball::ExtractTargetID(std::vector<std::string> &expTokens) {
	std::string token;
	std::vector<std::string> temp;
	unsigned int j = 0;
	bool hasID = false;

	while(j < expTokens.size()) {
		if(expTokens.at(j).compare("<-") == 0) {
			token = expTokens[0];
	
			unsigned int i;
			for(i = 2; i < expTokens.size(); i++) {
				temp.push_back(expTokens[i]);
			}
			expTokens = temp;
			hasID = true;
		}
		j++;
	}
	if(!hasID) {
		token = ":last";
	}

	return token;
}

unsigned int Baseball::Evaluate(const std::vector<std::string> &expTokens) {
	unsigned int value;

	if(expTokens.size() == 1) {
		value = EvaluateTerm(expTokens[0]);
	} else { // size 3
		value = EvaluateOperation(expTokens[0], expTokens[1], expTokens[2]);
	}

	return value;
}

std::string Baseball::Evaluate(const std::string &exp) {
	std::vector<std::string> tokens = Tokenize(exp);
	std::string variable;
	unsigned int base;
	unsigned int value;
	std::string stringInBase;	
	
	variable = ExtractTargetID(tokens);
	base = ExtractTargetBase(tokens);
	value = Evaluate(tokens);

	// save the value into the variable
	varmap[variable] = value;

	// convert to desired base
	stringInBase = ToBase(value, base);

	return stringInBase;
}
